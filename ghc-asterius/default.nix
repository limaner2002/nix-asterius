{ asterius
, autoconf
, automake
, ghc
, ghc-asterius_patched
, gmp
, haskell
, haskellPackages
, lib
, perl
, python3
, stdenv
}:
let hadrian = haskellPackages.callCabal2nix "hadrian" "${ghc-asterius_patched}/hadrian" {};
    autoGen = import ./autogen.nix { inherit lib; };
in stdenv.mkDerivation {
      src = "${ghc}";
      name = "ghc-asterius";
      phases = [ "unpackPhase"
                 "configurePhase"
                 "buildPhase"
                 "installPhase"
               ];
      configurePhase = ''
        python ./boot --hadrian && ./configure
      '';
      buildPhase = ''
        echo "**************************************************** hadrian ****************************************************"
        hadrian -j "${autoGen.ghc_autogen_files}" "${autoGen.ghc_pkg_autogen_files}"
      '';
      installPhase = ''
        mkdir $out
        cp -r _build/* $out
      '';

      buildInputs = [ python3
                      autoconf
                      automake
                      gmp
                      hadrian
                      haskell.compiler.ghc884
                      haskell.packages.ghc884.alex
                      haskell.packages.ghc884.happy
                      perl
                    ];
      haskellCompilerName = "ghc-8.8.4";
      version = "8.8.4";
      targetPrefix = "";
    }
