{ asterius-autogen
, haskell
, haskellPackages
, inline-js
, nix
, stdenv
}:
rec { ghc-asterius = haskellPackages.callCabal2nix "ghc-asterius" "${asterius-autogen}/lib/ghc-asterius"
      { inherit
        ghc-boot-asterius
        ghc-boot-th-asterius
        ghc-heap-asterius
        ghci-asterius
        template-haskell-asterius;
      };
      ghc-boot-asterius = haskellPackages.callCabal2nix "ghc-boot-asterius" "${asterius-autogen}/lib/ghc-boot-asterius"
        { inherit ghc-boot-th-asterius;
        };
      ghc-boot-th-asterius = haskellPackages.callCabal2nix "ghc-boot-th-asterius" "${asterius-autogen}/lib/ghc-boot-th-asterius" {};
      ghc-heap-asterius = haskellPackages.callCabal2nix "ghc-heap-asterius" "${asterius-autogen}/lib/ghc-heap-asterius" {};
      ghci-asterius = haskellPackages.callCabal2nix "ghci-asterius" "${asterius-autogen}/lib/ghci-asterius"
        { inherit
          ghc-boot-asterius
          ghc-boot-th-asterius
          ghc-heap-asterius
          template-haskell-asterius;
        };
      ghc-toolkit = haskellPackages.callCabal2nix "ghc-toolkit" "${asterius-autogen}/ghc-toolkit"
        { inherit ghc-asterius;
        };
      inline-js-core = haskellPackages.callCabal2nix "inline-js-core" "${inline-js}/inline-js-core" {};
      asterius-with-tests = haskellPackages.callCabal2nix "asterius" "${asterius-autogen}/asterius"
              { inherit
                ghc-asterius
                ghci-asterius
                ghc-boot-asterius
                ghc-toolkit
                inline-js-core
                template-haskell-asterius
                wasm-toolkit;
              };
      # For now the tests are getting stuck in some sort of loop and
      # had to be killed after 4 hours. As a result, this is the
      # default package when building the flake.
      asterius = haskell.lib.dontCheck (haskell.lib.dontStrip (haskell.lib.dontHaddock asterius-with-tests));
      asterius-patched = asterius.overrideAttrs (old:
        { patches = [ ./patches/BuildInfo.hs.patch ];
        }
      );
      # Used purely for debugging the output after build and before installation.
      asterius-preboot = asterius.overrideAttrs (old:
        { preInstallPhases = [];
          installPhase = ''
            mkdir $out
            cp -r ./* $out
          '';
          fixupPhase = "";
          postInstall = "";
          haddockPhase = ''
            echo "**************************************************** No Haddock ****************************************************"
          '';
          checkPhase = "";
          postFixup = "";
          outputs = ["out"];
        }
      );
      template-haskell-asterius = haskellPackages.callCabal2nix "template-haskell-asterius" "${asterius-autogen}/lib/template-haskell-asterius"
          { inherit
            ghc-boot-th-asterius;
          };
      wasm-toolkit = haskellPackages.callCabal2nix "wasm-toolkin" "${asterius-autogen}/wasm-toolkit" {};
    }
