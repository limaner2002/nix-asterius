{
  description = "A very basic flake";

  inputs =
    { haskellSrc.url = github:input-output-hk/haskell.nix;
      nixpkgs.url = github:NixOS/nixpkgs/nixos-20.09;
      asterius =
        { url = github:tweag/asterius;
          flake = false;
        };
      ghc_src =
        { url = github:TerrorJack/ghc/asterius-8.8;
          flake = false;
        };
      inline-js =
        { url = github:tweag/inline-js?rev=ef675745e84d23d51c50660d40acf9e684fbb2d6;
          flake = false;
        };
    };

  outputs = { asterius, ghc_src, haskellSrc, inline-js, self, nixpkgs }:
    let mkPkgs = system:
          let haskell-nix = import haskellSrc { inherit system; };
              pkgs = import nixpkgs { inherit system;
                                    };
              lts = pkgs.haskell-nix.snapshots."lts-16.29";
              patchStack = pkgs.stdenv.mkDerivation {
                name = "asterius-patched-src";
                phases = [ "unpackPhase"
                           "patchPhase"
                           "buildPhase"
                           "installPhase" ];
                unpackPhase = ''
                  cp -r --no-preserve=mode $src/* .
                  mkdir -p lib/ghc
                  cp -r --no-preserve=mode ${ghc}/* lib/ghc
                  chmod +x lib/ghc/boot
                  chmod +x lib/ghc/hadrian/build.stack.sh
                  chmod +x lib/ghc/hadrian/build.stack.nix.sh
                '';
                patchPhase = ''
                  patch stack.yaml ${self}/asterius/patches/stack.yaml.patch
                  patch utils/make-packages.py ${patches}
                  patchShebangs .
                '';
                buildPhase = ''
                  pushd lib
                  # ls -lh /build/lib/ghc/hadrian/build.stack.sh
                  # python ../utils/make-packages.py
                  popd
                '';
                installPhase = ''
                  mkdir $out
                  cp -r . $out
                '';
                buildInputs = [ pkgs.patch
                                pkgs.python3
                                pkgs.haskellPackages.stack
                              ];
                src = asterius;
              };
              patches = pkgs.callPackage "${self}/asterius/patches.nix" { inherit self; };
              ghc = pkgs.fetchgit {
                url = "https://github.com/TerrorJack/ghc";
                rev = ghc_src.rev;
                fetchSubmodules = true;
                sha256 = "rHGPtZD2CcEaV/6OuNifZFQ9O2uKv0VasmzZb7m9KFk=";
              };
              ghc-asterius = pkgs.callPackage "${self}/asterius/ghc-asterius/default.nix" { inherit asterius ghc; };
          in rec { inherit ghc-asterius;
                   asterius-compiler = pkgs.callPackage "${self}/asterius/default.nix"
                     { inherit
                       asterius
                       ghc-asterius
                       inline-js;
                     };
                   asterius-autogen = pkgs.callPackage "${self}/asterius/impure/autogen.nix"
                     { inherit
                       asterius
                       ghc-asterius
                       hadrian
                       inline-js
                       nixpkgs;
                     };
                   asterius-impure_bootPackages = pkgs.callPackage "${self}/asterius/build-impure.nix"
                     { inherit
                       asterius-autogen
                       inline-js;
                     };
                   hadrian = pkgs.callPackage "${self}/asterius/ghc-asterius/hadrian.nix"
                     { inherit ghc-asterius_patched;
                     };
                   ghc-asterius_patched = pkgs.callPackage "${self}/asterius/ghc-asterius/patched-src.nix" { inherit asterius ghc; };
                   defaultPackage =
                     { "${system}" = asterius-impure_bootPackages.asterius-with-tests;
                     };
                   ghc-asterius-tar = pkgs.stdenv.mkDerivation
                     { name = "ghc-asterius.tar";
                       src = ghc;
                       phases = [ "unpackPhase"
                                  "packPhase"
                                ];
                       packPhase = ''
                         mkdir $out
                         tar -cf $out/ghc-asterius.tar ./*
                       '';
                     };
                 };
    in
      { packages =
          { x86_64-linux = mkPkgs "x86_64-linux";
            x86_64-darwin = mkPkgs "x86_64-darwin";
          };
      };
}
