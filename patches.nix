{ self
, writeText
}:
writeText "make-packages.py.patch"
''
--- /nix/store/azzkshwcj4miss7nwf87k544zs6pvsck-source/utils/make-packages.py    1969-12-31 19:00:01.000000000 -0500
+++ /tmp/make-packages.py    2021-01-16 18:13:00.340693634 -0500
@@ -10,7 +10,7 @@
 ghc_repo_branch = "asterius-8.8-staging"
 workdir = os.getcwd()
 ghc_repo_path = os.path.join(workdir, "ghc")
-hadrian_path = os.path.join(ghc_repo_path, "hadrian", "build.stack.sh")
+hadrian_path = os.path.join(ghc_repo_path, "hadrian", "build.stack.nix.sh")
 ghc_heap_asterius_path = os.path.join(workdir, "ghc-heap-asterius")
 ghc_boot_th_asterius_path = os.path.join(workdir, "ghc-boot-th-asterius")
 ghc_boot_asterius_path = os.path.join(workdir, "ghc-boot-asterius")
@@ -121,7 +121,7 @@

 def make_hadrian():
     patch_hadrian()
-    subprocess.run([hadrian_path, "--version"], cwd=ghc_repo_path, check=True)
+    subprocess.run([hadrian_path, "--version"], check=True)


 def make_autogen():
@@ -427,9 +427,7 @@


 if __name__ == "__main__":
-    ghc_checkout()
-    ghc_clean()
-    make_hadrian()
+    # make_hadrian()
     ghc_configure()
     make_autogen()
     make_ghc_heap_asterius()
''
