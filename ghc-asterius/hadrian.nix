{ haskellPackages
, ghc-asterius_patched
}:
haskellPackages.callCabal2nix "hadrian" "${ghc-asterius_patched}/hadrian" {}
