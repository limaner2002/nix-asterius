{ asterius
, cabal-install
, cacert
, ghc-asterius_patched
, git
, haskellPackages
, inline-js
, patch
, python3
, stdenv
}:
let make-packagePatch = ./patches/make-packages.py.patch;
    package-yamlPatch = ./patches/package.yaml.patch;
    ghcToolkit-yamlPatch = ./patches/ghc-toolkit/package.yaml.patch;
    setting-hsPatch = ./patches/Setting.hs.patch;
    packages = haskellPackages.override (old:
      { ghc = ghc-asterius_patched;
      }
    );
    ghc-toolkit = packages.callCabal2nix "ghc-toolkit" "${asterius_patched}/ghc-toolkit" {};
    asterius_patched = stdenv.mkDerivation
      { name = "asterius-patched";
        phases = [ "unpackPhase"
                   "patchPhase"
                   "installPhase"
                 ];
        patchPhase = ''
          patch asterius/package.yaml ${package-yamlPatch}
          patch ghc-toolkit/package.yaml ${ghcToolkit-yamlPatch}
        '';
        installPhase = ''
          mkdir $out
          cp -r ./* $out
        '';
        src = asterius;
        buildInputs = [ patch
                      ];
      };
    inline-js-core = packages.callCabal2nix "inline-js-core" "${inline-js}/inline-js-core" {};
    # inline-js = packages.callCabal2nix "inline-js" "${inline-js}/inline-js" { inherit inline-js-core; };
    wasm-toolkit = packages.callCabal2nix "wasm-toolkit" "${asterius}/wasm-toolkit" {};
in
packages.callCabal2nix "asterius" "${asterius_patched}/asterius"
  { inherit ghc-toolkit inline-js-core wasm-toolkit;
  }
