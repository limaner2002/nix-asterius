# This is an impure build version that almost follows the build guide
# for asterius. To run this build, it is necessary to enable the
# --no-sandbox option when calling nix-build or nix build. It was
# necessary to avoid the stack build of hadrian because the build user
# could not issue a connection to the nix daemon. As a result, hadrian
# is passed into this module and make-packages.py is patched to use it
# directly instead of performing a 'stack exec hadrian'.
{ asterius
, autoconf
, automake
, cabal-install
, cacert
, ghc-asterius
, ghc
, hadrian
, git
, haskellPackages
, inline-js
, nix
, nixpkgs
, patch
, python3
, writeText
, stack
, stdenv
}:
let make-packages_patch = ../patches/make-packages.py-impure.patch;
in
stdenv.mkDerivation {
  name = "asterius-autogen";
  phases = [ "unpackPhase"
             "patchPhase"
             "buildPhase"
             "installPhase"
           ];
  patchPhase = ''
    patchShebangs .
    patch utils/make-packages.py ${make-packages_patch}
  '';
  buildPhase = ''
    mkdir lib
    cd lib
    python ../utils/make-packages.py
    rm -rf ghc
    cd ../
  '';
  installPhase = ''
    mkdir $out
    cp -r ./* $out
  '';
  src = asterius;
  buildInputs = [ autoconf
                  automake
                  cacert
                  cabal-install
                  git
                  ghc
                  hadrian
                  haskellPackages.alex
                  haskellPackages.happy
                  nix
                  python3
                  stack
                ];
  NIX_PATH = "nixpkgs=${nixpkgs}";
}
