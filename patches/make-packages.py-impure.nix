{ nixpkgs
, writeText
}:
writeText "make-packages.py.patch"
''
--- /nix/store/azzkshwcj4miss7nwf87k544zs6pvsck-source/utils/make-packages.py	1969-12-31 19:00:01.000000000 -0500
+++ /tmp/make-packages.py	2021-02-07 17:37:07.761872111 -0500
@@ -121,11 +121,11 @@
 
 def make_hadrian():
     patch_hadrian()
-    subprocess.run([hadrian_path, "--version"], cwd=ghc_repo_path, check=True)
+    subprocess.run([hadrian_path, "-Inixpkgs=${nixpkgs}", "--version"], cwd=ghc_repo_path, check=True)
 
 
 def make_autogen():
-    subprocess.run([hadrian_path, "-j"] + ghc_autogen_files +
+    subprocess.run([hadrian_path, "-Inixpkgs=${nixpkgs}", "-j"] + ghc_autogen_files +
                    ghc_pkg_autogen_files,
                    cwd=ghc_repo_path,
                    check=True)
''
