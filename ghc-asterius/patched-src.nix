{ asterius
, stdenv
, ghc
}:
let setting-hsPatch = ../patches/Setting.hs.patch;
in stdenv.mkDerivation {
      name = "ghc-asterius-patched";
      src = ghc;
      phases = [ "unpackPhase"
                 "patchPhase"
                 "installPhase"
               ];
      patchPhase = ''
        cp ${asterius}/utils/UserSettings.hs hadrian/
        patch hadrian/src/Oracles/Setting.hs ${setting-hsPatch};
        patchShebangs .
      '';
      installPhase = ''
        mkdir $out
        cp -r ./* $out
      '';
    }
